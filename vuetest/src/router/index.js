import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Book from "../views/Book";
import Page1 from "../views/Page1";
import Page2 from "../views/Page2";
import Page3 from "../views/Page3";
import Page4 from "../views/Page4";
import Index from "../views/Index";
import Login from "../views/Login";

Vue.use(VueRouter)

//路由
const routes = [
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home,
  //   children
  // },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // },
  // {
  //   path:"/book",
  //   component: Book
  // },
  {
    path: '/login',
    name: 'Login',
    component: ()=> import('../views/Login')
  },
  {
      path:"/",
      name:'图书管理',
      component:Index,
      redirect:"/Page1",
      children:[
        {
          path:"/Page1",
          name:"查询图书",
          component:Page1,
          meta:{
            title: 'page1', //菜单名称
            roles: ['user', 'admin'], //当前菜单哪些角色可以看到
            icon: 'el-icon-info' //菜单左侧的icon图标
          }
        },
        {
          path:"/Page2",
          name:"添加图书",
          component:Page2
        }
      ]
  },
  {
    path:"/n2",
    name:'菜单2',
    component:Index,
    children:[
      {
        path:"/Page3",
        name:"页面3",
        component:Page3
      },
      {
        path:"/Page4",
        name:"页面4",
        component:Page4
      }
    ]
  }
  // {
  //     path:"/Page1",
  //     name:"页面1",
  //     component:Page1
  // },
  // {
  //   path:"/Page2",
  //   name:"页面2",
  //   component:Page2
  // },
  // {
  //   path:"/Page3",
  //   name:"页面3",
  //   component:Page3
  // },
  // {
  //   path:"/Page4",
  //   name:"页面4",
  //   component:Page4
  // }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
